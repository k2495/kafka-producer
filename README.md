
# kafka-producer

The purpose of this application is to put hands on how to interact with Kafka.

This application is part of a series and cover the event production operations.

The context of this application is the following:

* Consider it simulate the production user contact operations by multiple sources.
* Those operations are categorized by one of the following verbs `CREATE`, `UPDATE` or `DELETE`.
* They also hold the source reference for further analysis.
* Otherwise, they hold the card data, as exposed in the event description section.

## Overview of the process

### Kafka's topic initialization process

Out of the box, this application will check if the configured topic exists, creates it if not.

The topic's name is configured through the property `topic.name` which is mandatory. It should be defined through the
environment variable `TOPIC_NAME` whenever the application is used from a container, i.e. Docker or Kubernetes.

### Application main loop

Once the topic has been initialized, the application is able to send events towards it.
This process is implemented by a task scheduled with a delay configured by `application.heartbeat-in-ms` (by default `1000`).

The task rely on a service supplying events fed with random data. Don't expect them to fully correlated !

The serialization operation is a custom implementation, see `CustomSerializer`.

### Log Management

By default, the log level is `ERROR`. For a more detailed description of the process, you may set it to `TRACE`, 
with the environment variable `LOGGING_LEVEL_COM_SFEIR_MBODIN`.

By default, logs go on the standard output, i.e. not a file. 

## Key description

The key are produced by the concatenation of the source of the event with the suffix `_raw_operation`.

## Event description

The events that will be produced are contact cards operations.

They look like the following:

```json 
{
  "operation": "CREATE",
  "source": "6111C75B-BC17-4544-9512-E9F1B380E38E"
  "firstname": "John",
  "lastname": "DOE",
  "nickname": "jdoe",
  "birthday": "1970-01-01",
  "emails": [
    {
      "label": "work",
      "value": "doe.j@sfeir.com",
      "isDefault": false
    }
  ]
  "phones": [
    {
      "label": "personal",
      "value": "+33123456789",
      "isDefault": false
    }
  ],
  "addresses": [
    {
      "label": "work",
      "street": "1 avenue de l'Europe",
      "locality": "Shiltigheim",
      "region": "Alsace",
      "postcode": "67300",
      "country": "France",
      "isDefault": false
    }
  ]
}
```

Check the source code for the constraints against each field.

You will find the Protobuf definition in `src/main/proto/producer.proto`

## Configuration

Check the file `application.properties` to get a complete overview of the configuration. Most have default value,
nevertheless some are mandatory:

* `application.bootstrap-servers` which represent where Kafka is reachable, expressed as `host:port`.
* `topic.name` which represent the name of the topic events will be produced to.

Keep in mind, it is often preferable to feed properties through environment variables for containerized runtime.

## Build the application as a container image

With `docker`:

```shell
./mvnw compile jib:dockerBuild
```

With `podman`:

```shell
./mvnw compile jib:dockerBuild -Djib.dockerClient.executable=$(which podman)
```

## Run the image

Assuming the following:

* Kafka is running on a dedicated network `kafka_default`
* Kafka is reachable at `kafka:9092`

With `podman`:

```shell
podman run -it --rm -e TOPIC_NAME=private_producer -e APPLICATION_BOOTSTRAP_SERVERS=kafka:9092 --network kafka_default localhost/kafka-producer:1.0.0
```