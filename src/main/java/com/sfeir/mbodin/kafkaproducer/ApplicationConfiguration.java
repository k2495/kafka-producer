package com.sfeir.mbodin.kafkaproducer;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Configuration
@ConfigurationProperties(prefix = "application")
@Setter
@Getter
public class ApplicationConfiguration {
    @NotBlank
    private String bootstrapServers;

    @NotBlank
    private String clientId;

    @Positive
    private long heartbeatInMs;
}
