package com.sfeir.mbodin.kafkaproducer.topic;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.concurrent.ExecutionException;

@Component
@Slf4j
public class TopicInitializer {
    @Autowired
    private TopicConfiguration configuration;

    public boolean isInitialized() {
        boolean exists = false;

        try (final Admin admin = Admin.create(configuration.asProperties())) {
            if (log.isTraceEnabled()) log.trace("Topic \"{}\" is being checked", configuration.getName());

            exists = admin.listTopics().names()
                    .thenApply(names -> names.contains(configuration.getName()))
                    .get();

            if (!exists) {
                if (log.isTraceEnabled()) log.trace("Topic \"{}\" need to be created", configuration.getName());
                final NewTopic topic = new NewTopic(configuration.getName(), configuration.getNumPartition(), configuration.getReplicationFactor())
                        .configs(configuration.asCreatingMap());

                admin.createTopics(Collections.singleton(topic)).all().get();

                if (log.isTraceEnabled()) log.trace("Topic \"{}\" has been created", configuration.getName());

                exists = true;
            } else if (log.isTraceEnabled()) log.trace("Topic \"{}\" already exists", configuration.getName());

        } catch (RuntimeException | InterruptedException | ExecutionException exception) {
            log.error("Initialization failed", exception);
        }

        return exists;
    }

    public String getName() {
        return configuration.getName();
    }
}
