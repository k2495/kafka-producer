package com.sfeir.mbodin.kafkaproducer.topic;

import com.sfeir.mbodin.kafkaproducer.ApplicationConfiguration;
import lombok.Getter;
import lombok.Setter;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ConfigurationProperties(prefix = "topic")
public class TopicConfiguration {

    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    @Getter
    @Setter
    @NotBlank
    private String name;
    @Getter
    @Setter
    @Positive
    private Integer numPartition;
    @Getter
    @Setter
    private Short replicationFactor;
    @Setter
    @Positive
    private Long retentionInByte;

    public Properties asProperties() {
        final Properties properties = new Properties();

        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, applicationConfiguration.getBootstrapServers());
        return properties;
    }

    public Map<String, String> asCreatingMap() {
        final Map<String, String> configs = new HashMap<>(){};

        // Configure retention by size on disk
        configs.put("retention.bytes", retentionInByte.toString());
        configs.put("segment.bytes", String.valueOf(retentionInByte / 5));

        return configs;
    }
}
