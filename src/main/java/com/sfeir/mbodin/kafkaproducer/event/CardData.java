package com.sfeir.mbodin.kafkaproducer.event;

import com.google.protobuf.Message;
import com.sfeir.mbodin.kafkaproducer.SerializationUtil;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Builder
@Getter
@Setter
public class CardData {
    public static final SerializationUtil.FieldMapper FIRSTNAME_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.CardData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.CardData.FIRSTNAME_FIELD_NUMBER)
            , Function.identity()
            );

    public static final SerializationUtil.FieldMapper LASTNAME_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.CardData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.CardData.LASTNAME_FIELD_NUMBER)
            , Function.identity()
            );

    public static final SerializationUtil.FieldMapper BIRTHDAY_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.CardData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.CardData.BIRTHDAY_FIELD_NUMBER)
            , Function.identity()
            );

    public static final SerializationUtil.FieldMapper EMAILS_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.CardData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.CardData.EMAILS_FIELD_NUMBER)
            , value -> ((EmailData)value).toMessage()
            );

    public static final SerializationUtil.FieldMapper PHONES_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.CardData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.CardData.PHONES_FIELD_NUMBER)
            , value -> ((PhoneData)value).toMessage()
            );

    public static final SerializationUtil.FieldMapper ADDRESSES_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.CardData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.CardData.ADDRESSES_FIELD_NUMBER)
            , value -> ((AddressData)value).toMessage()
            );

    private String firstname;
    private String lastname;
    @NotBlank
    private String nickname;
    private String birthday;
    @Builder.Default
    private List<EmailData> emails = new ArrayList<>();
    @Builder.Default
    private List<PhoneData> phones = new ArrayList<>();
    @Builder.Default
    private List<AddressData> addresses = new ArrayList<>();

    public Message toMessage() {
        final com.sfeir.mbodin.kafkaproducer.protobuff.CardData.Builder builder = com.sfeir.mbodin.kafkaproducer.protobuff.CardData.newBuilder();

        SerializationUtil.addIfDefined(builder, FIRSTNAME_DESCRIPTOR, firstname);
        SerializationUtil.addIfDefined(builder, LASTNAME_DESCRIPTOR, lastname);
        SerializationUtil.addIfDefined(builder, BIRTHDAY_DESCRIPTOR, birthday);
        SerializationUtil.addIfDefined(builder, EMAILS_DESCRIPTOR, emails);
        SerializationUtil.addIfDefined(builder, PHONES_DESCRIPTOR, phones);
        SerializationUtil.addIfDefined(builder, ADDRESSES_DESCRIPTOR, addresses);

        return builder.setNickname(nickname).build();
    }
}
