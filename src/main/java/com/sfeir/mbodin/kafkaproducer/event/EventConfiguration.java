package com.sfeir.mbodin.kafkaproducer.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.time.Duration;
import java.util.Locale;

@Configuration
@ConfigurationProperties(prefix = "event")
public class EventConfiguration {
    @Getter
    @Setter
    private Locale locale;
}
