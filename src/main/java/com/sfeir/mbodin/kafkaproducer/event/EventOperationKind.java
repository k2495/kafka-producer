package com.sfeir.mbodin.kafkaproducer.event;

public enum EventOperationKind {
    CREATE(1),
    UPDATE(2),
    DELETE(3);

    private final int operationCode;

    EventOperationKind(int code) {
        operationCode = code;
    }

    public com.sfeir.mbodin.kafkaproducer.protobuff.EventOperationKind toEnumValue() {
        switch (this) {
            case CREATE:
                return com.sfeir.mbodin.kafkaproducer.protobuff.EventOperationKind.CREATE;
            case UPDATE:
                return com.sfeir.mbodin.kafkaproducer.protobuff.EventOperationKind.UPDATE;
            case DELETE:
                return com.sfeir.mbodin.kafkaproducer.protobuff.EventOperationKind.DELETE;
            default:
                return com.sfeir.mbodin.kafkaproducer.protobuff.EventOperationKind.UNRECOGNIZED;
        }
    }
}
