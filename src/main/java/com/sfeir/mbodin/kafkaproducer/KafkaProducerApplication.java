package com.sfeir.mbodin.kafkaproducer;

import com.google.protobuf.Message;
import com.sfeir.mbodin.kafkaproducer.event.EventDataService;
import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import com.sfeir.mbodin.kafkaproducer.topic.TopicInitializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.text.MessageFormat;
import java.util.Properties;

@SpringBootApplication
@Slf4j
public class KafkaProducerApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(KafkaProducerApplication.class, args);
    }

    @Autowired
    private TopicInitializer topic;

    @Autowired
    private EventDataService eventDataService;

    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        final ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(5);
        threadPoolTaskScheduler.setThreadNamePrefix("kafka-event-producer");
        return threadPoolTaskScheduler;
    }

    @Bean
    @Scope
    Properties producerProperties() {
        final Properties properties = new Properties();

        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, applicationConfiguration.getBootstrapServers());
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, applicationConfiguration.getClientId());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CustomSerializer.class);

        return properties;
    }

    @Override
    public void run(ApplicationArguments args) {
        if (topic.isInitialized()) {
            log.trace("Start producing message");
            threadPoolTaskScheduler().scheduleWithFixedDelay(buildTask(), applicationConfiguration.getHeartbeatInMs());
        } else {
            log.error("Topic is missing");
        }
    }

    private Runnable buildTask() {
        return () -> {
            final Message message = eventDataService.getNext();
            final Object field = message.getField(EventData.getDescriptor().findFieldByNumber(EventData.SOURCE_FIELD_NUMBER));
            final String key = MessageFormat.format("{0}_raw_operation", field);

            final ProducerRecord<String, Message> record = new ProducerRecord<>(topic.getName(), key, message);

            try (KafkaProducer<String, Message> kafkaProducer = new KafkaProducer<>(producerProperties())) {
                kafkaProducer.send(record, (metadata, exception) -> {
                    if (exception != null) {
                        log.error("", exception);
                    } else if (log.isTraceEnabled()) {
                        log.trace("The message has been sent:\n{}", record.value().toString());
                    }
                });
            }
        };
    }
}
